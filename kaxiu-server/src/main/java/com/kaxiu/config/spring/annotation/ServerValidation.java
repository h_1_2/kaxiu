package com.kaxiu.config.spring.annotation;

import com.kaxiu.vo.enums.AttestationEnum;

import java.lang.annotation.*;

/**
 * @author: LiYang
 * @create: 2019-08-15 22:14
 * @Description:
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServerValidation {

    // 检查是否删除
    boolean deleted() default false;

    // 默认status
    String status() default "1";

    // 认证状态
    AttestationEnum auditStatus() default AttestationEnum.CERTIFIED;

}
