//package com.kaxiu.config.artemis;
//
//import com.kaxiu.config.mqtt.MqttProperties;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jms.annotation.JmsListener;
//import org.springframework.jms.core.JmsMessagingTemplate;
//import org.springframework.stereotype.Component;
//
//import javax.jms.Destination;
//
///**
// * @author: LiYang
// * @create: 2019-08-10 01:23
// * @Description:
// **/
//@Component
//public class Producer {
//
//    @Autowired
//    private JmsMessagingTemplate jmsMessagingTemplate;
//
//    public void send(Destination destination, final String message) {
//        jmsMessagingTemplate.convertAndSend(destination, message + "from queue");
//    }
//
//    @JmsListener(destination= "topic")
//    public void consumerMessage(String text){
//        System.out.println("从out.queue队列收到的回复信息为:"+text);
//    }
//}
