//package com.kaxiu.config.artemis;
//
//import org.springframework.jms.annotation.JmsListener;
//import org.springframework.messaging.handler.annotation.SendTo;
//import org.springframework.stereotype.Component;
//
///**
// * @author: LiYang
// * @create: 2019-08-10 01:23
// * @Description:
// **/
//@Component
//public class Consumer {
//
//    @JmsListener(destination = "mytest.queue")
//    @SendTo("out.queue")
//    public String receiveQueue(String text) {
//        System.out.println("Consumer收到的信息为:"+text);
//        return "return message "+text;
//    }
//
//}
