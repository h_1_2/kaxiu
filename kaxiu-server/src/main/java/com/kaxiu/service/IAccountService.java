package com.kaxiu.service;

import com.kaxiu.persistent.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kaxiu.persistent.entity.Orders;

/**
 * <p>
 * 资金账户表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IAccountService extends IService<Account> {

    Account getByUserId();

    /**
     * 相关表：
     *      - account
     *          - 管理员账户 id = 1
     *          - 中间账户 id = 2
     *      - account_log
     * 1. 用户付款成功
     *      - 用户账户根据支付来源处理用户账户，暂时只有微信直接支付，所以用户account表不需修改
     *      - 中间账户入账这笔费用，作为临时保存款项处理， 其他账户暂时不会有这笔款项的记录
     *      - 日志表记录
     *          - 用户支出一笔款项
     *          - 中间账户入账
     * 2. 用户取消订单
     *      - 该笔订单至为取消状态
     *      — 中间账户出款，退款至微信账户
     *      - 日志表记录
     *          - 该笔订单日志记录至为取消
     *          - 中间账户出账
     * 3. 维修完成
     *      - 中间账户出账
     *      - 管理员账户入账
     *      - 维修人员账户入账
     *      - 日志表
     *          -中间账户出账
     *          -管理员账户入账
     *          -维修人员账户入账
     * 4. 维修人员提现（审核成功）
     *      - 维修人员账户出账
     *      日志表
     *          - 维修人员账户出账
     */

    Boolean successPayOrder(Orders orders);

    Boolean calcleOrder(Orders orders);

    Boolean overOrder(Orders orders);

    Boolean withdrawOrder();



}
