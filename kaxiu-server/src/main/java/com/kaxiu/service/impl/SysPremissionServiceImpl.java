package com.kaxiu.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kaxiu.persistent.entity.SysPremission;
import com.kaxiu.persistent.mapper.SysPremissionMapper;
import com.kaxiu.service.ISysPremissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class SysPremissionServiceImpl extends ServiceImpl<SysPremissionMapper, SysPremission> implements ISysPremissionService {

    @Autowired
    private SysPremissionMapper mapper;

    @Override
    public List<SysPremission> getByRoleId(Long rolid) {
        return mapper.selectByRoleId(rolid);
    }
}
