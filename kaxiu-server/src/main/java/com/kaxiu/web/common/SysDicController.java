package com.kaxiu.web.common;


import com.kaxiu.common.base.AbstractBaseController;
import com.kaxiu.service.ISysDicService;
import com.kaxiu.vo.ResultDataWrap;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@RestController
@RequestMapping("/kaxiu/sys-dic")
public class SysDicController extends AbstractBaseController {

    @Resource
    private ISysDicService sysDicService;

    @PostMapping("")
    public ResultDataWrap getDic(@RequestBody String[] dics){
        return buildResult(sysDicService.getDicByArr(dics));
    }


}

