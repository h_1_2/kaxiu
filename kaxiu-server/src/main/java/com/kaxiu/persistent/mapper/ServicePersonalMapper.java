package com.kaxiu.persistent.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kaxiu.persistent.entity.ServicePersonal;

/**
 * <p>
 * 维修人员信息表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface ServicePersonalMapper extends BaseMapper<ServicePersonal> {

}
