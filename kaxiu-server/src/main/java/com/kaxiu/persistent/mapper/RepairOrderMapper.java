package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.RepairOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 维修单详情 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface RepairOrderMapper extends BaseMapper<RepairOrder> {

}
