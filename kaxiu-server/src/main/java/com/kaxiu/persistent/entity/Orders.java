package com.kaxiu.persistent.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Orders extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 用户表id
     */
    private Long userId;

    /**
     * 价格
     */
    private BigDecimal amount;

    /**
     * 实际支付价格
     */
    private BigDecimal realAmount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 0:未支付，1:已支付,2:已关闭;3,支付失败
     */
    private Integer payStatus;

    /**
     * 支付交易id
     */
    private String transactionId;

    /**
     * 订单完成时间
     */
    private LocalDateTime finishedTime;

    /**
     * 订单类型:维修订单
     */
    private Integer orderType;


}
