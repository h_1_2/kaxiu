package com.kaxiu.vo.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author: LiYang
 * @create: 2019-08-05 20:15
 * @Description: 状态，1：发起订单，未支付，2：已支付，等待接单，3：已接单，正在赶往现场，4：维修完成，5：取消订单
 **/
public enum AttestationEnum {

    CERTIFIED(1, "已认证"),
    UN_CERTIFIED(0, "未认证"),
    CERTIFICATING(3, "认证中");

    @EnumValue
    private Integer value;

    @TableField(exist = false)
    private String desc;

    AttestationEnum(final Integer value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return this.value;
    }
}
