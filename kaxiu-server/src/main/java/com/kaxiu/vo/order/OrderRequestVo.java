package com.kaxiu.vo.order;

import com.alibaba.fastjson.JSONObject;
import com.kaxiu.persistent.entity.OrderItem;
import com.kaxiu.persistent.entity.Orders;
import com.kaxiu.persistent.entity.RepairOrder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author: LiYang
 * @create: 2019-08-04 21:42
 * @Description:
 **/
@Data
public class OrderRequestVo {

    private JSONObject location;
    private String[] photos;
    private String remark;
    private String serviceFee;
    private String serviceId;
    private String totalFee;
    private String contact;
    private String mobile;

    public Orders vo2Orders(){
        Orders orders = new Orders();
        orders.setAmount(new BigDecimal(this.getTotalFee()));
        orders.setRealAmount(new BigDecimal(this.getTotalFee()));
        return orders;
    }

    public OrderItem vo2OrderItem(){
        OrderItem item = new OrderItem();
        item.setProductId(Long.valueOf(this.getServiceId()));
        item.setAmount(new BigDecimal(this.getTotalFee()));
        return item;
    }

    public RepairOrder vo2RepairOrder(){
        RepairOrder order = new RepairOrder();
        order.setServiceId(Long.valueOf(this.getServiceId()));
        order.setAmount(new BigDecimal(this.getTotalFee()));
        order.setScenePhoto(Arrays.stream(this.getPhotos()).collect(Collectors.joining(",")));
        order.setRemark(this.getRemark());
        order.setLocation(this.getLocation());
        order.setContact(this.getContact());
        order.setMobile(this.getMobile());
        return order;
    }

}
